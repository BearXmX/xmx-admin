/*
 * @Description:
 * @Author: mingxiangxiong
 * @Date: 2021-03-02 21:06:04
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-08-20 20:11:29
 */
module.exports = {
    devServer: {
        open: true,
        proxy: {
            '/get': {
                target: 'http://localhost:3000', // 接口服务器域名地址
                changeOrigin: true,
                pathReWrite: { '/get': '/get' },
            },
            '/mysql': {
                target: 'http://localhost:3001', // 接口服务器域名地址
                changeOrigin: true,
                pathReWrite: { '/mysql': '/mysql' },
            },
        },
    },
};
