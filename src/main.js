/*
 * @Description:
 * @Author: mingxiangxiong
 * @Date: 2021-02-10 17:40:04
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-08-01 00:31:36
 */
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './plugins/iview.js';
import './style/index.css';
import axios from 'axios';

Vue.config.productionTip = false;
Vue.prototype.$axios = axios;
Vue.prototype.$ENV = process.env.NODE_ENV
new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount('#app');
