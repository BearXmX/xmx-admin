/*
 * @Description:
 * @Author: mingxiangxiong
 * @Date: 2021-02-10 17:40:04
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-08-14 14:14:38
 */
import Vue from 'vue';
import VueRouter from 'vue-router';
import home from '@/views/home/index';
import cockpit from '@/views/home/cockpit/index';
import userCenter from '@/views/home/userCenter/index';
import userList from '@/views/home/userList/index'
import creatUser from '@/views/home/userList/creatUser'
import notPageFound from '@/views/404.vue';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        redirect: '/home',
    },
    {
        path: '/home',
        component: home,
        redirect: '/home/cockpit',
        children: [
            {
                path: '/home/cockpit',
                name: 'cockpit',
                component: cockpit,
                meta: {
                    title: '驾驶舱',
                },
            },
            {
                path: '/home/userCenter',
                name: 'userCenter',
                component: userCenter,
                meta: {
                    title: '用户中心',
                },
            },
            {
                path: '/home/userList',
                name: 'userList',
                component: userList,
                meta: {
                    title: '用户列表',
                },
            },
            {
                path: '/home/userList/creatUser',
                name: 'creatUser',
                component: creatUser,
                meta: {
                    title: '新建用户',
                },
            },
        ],
    },
    {
        path: '/login',
        name: 'login',
        meta: {
            title: '登录',
        },
        component: () => import('../views/login.vue'),
    },
    {
        path: '/404',
        name: '404',
        meta: {
            title: '404页面不存在',
        },
        component: notPageFound,
    },
    {
        path: '*', // 页面不存在的情况下会跳到404页面
        redirect: '/404',
        name: 'notFound',
        hidden: true,
    },
];

const router = new VueRouter({
    routes,
});

router.beforeEach((to, from, next) => {
    let token = sessionStorage.getItem('XmXtoken');
    if (token) {
        if (to.path == '/login') {
            next('/home');
        } else {
            next();
        }
    } else {
        if (to.path == '/login') {
            next();
        } else {
            next('/login');
        }
    }
});
router.afterEach((to, from, next) => {
    window.document.title = to.meta.title ? to.meta.title : '熊先生的后台';
});
export default router;
