import axios from 'axios'
// import { getToken, removeToken } from './auth' // 验权
import { Message } from 'iview'
// 正式环境
// 创建一个 axios 实例
const service = axios.create({
  baseURL: 'http://localhost:3000/',
  timeout: 20000 // 请求超时时间
})

// 请求拦截器
service.interceptors.request.use(
  config => {
    return config
  },
  error => {
    // 发送失败
    Promise.reject(error)
  }
)

// 响应拦截器
service.interceptors.response.use(
  response => {
    // dataAxios 是 axios 返回数据中的 data
    const dataAxios = response.data
    // 这个状态码是和后端约定的
    const { code } = dataAxios
    // 根据 code 进行判断
    if (code === undefined) {
      // 如果没有 code 代表这不是项目后端开发的接口
      return dataAxios
    } else {
      // 有 code 代表这是一个后端接口 可以进行进一步的判断
      switch (code) {
        case '00000': // SUCCESS
          // [ 示例 ] code === 0 代表没有错误
          // console.log('dataAxios', dataAxios)
          return response
        case '0001': // 处理失败
          // [ 示例 ] 其它和后台约定的 code
          // errorCreate(`[ code: 100000 ] ${dataAxios.message}: ${response.config.url}`, dataAxios.message);
          Message.error(dataAxios.message)
          break
        case '2000': // 缺少参数
          // [ 示例 ] 其它和后台约定的 code
          Message.error(dataAxios.message)
          break
        case '2001': // 无效参数
          // [ 示例 ] 其它和后台约定的 code
          Message.error(dataAxios.message)
          break
        case '2002': // 参数值错误
          // [ 示例 ] 其它和后台约定的 code
          Message.error(dataAxios.message)
          break
        case '2003': // 未知参数错误
          // [ 示例 ] 其它和后台约定的 code
          Message.error(dataAxios.message)
          break
        case '3000': // 未登录
          // [ 示例 ] 其它和后台约定的 code
          Message.error(dataAxios.message)
          break
        case '3001': // 未授权操作
          // [ 示例 ] 其它和后台约定的 code
          Message.error(dataAxios.message)
          break
        case '3002': // 无效token
          // [ 示例 ] 其它和后台约定的 code
          Message.error(dataAxios.message)
          break
        case '3003': // token错误
          // [ 示例 ] 其它和后台约定的 code
          Message.error(dataAxios.message)
          break
        case '4000': // 资源被占用
          // [ 示例 ] 其它和后台约定的 code
          Message.error(dataAxios.message)
          break
        case '4001': // 资源不足
          // [ 示例 ] 其它和后台约定的 code
          Message.error(dataAxios.message)
          break
        case '4002': // 资源不存在
          // [ 示例 ] 其它和后台约定的 code
          Message.error(dataAxios.message)
          break
        case '4003': // 资源不可用
          // [ 示例 ] 其它和后台约定的 code
          Message.error(dataAxios.message)
          break
        case '5000': // 系统内部异常
          // [ 示例 ] 其它和后台约定的 code
          Message.error(dataAxios.message)
          break
        case '6000': // 外部调用异常
          // [ 示例 ] 其它和后台约定的 code
          Message.error(dataAxios.message)
          break
        case '404': // 参数校验失败
          // [ 示例 ] 其它和后台约定的 code
          Message.error(dataAxios.message)
          break
        case '403': // 没有相关权限
          // [ 示例 ] 其它和后台约定的 code
          Message.error(dataAxios.message)
          break
        default:
          // 其他
          // 不是正确的 code
          //Message.error(dataAxios.message)
          break
      }
    }
    return response
  },
  error => {
    return error
  }
)

export default service