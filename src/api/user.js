/*
 * @Date: 2021-08-13 22:25:22
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-08-14 16:57:51
 * @Description:
 */
import mysql from '../server/mysql';

export function getUserInfo() {
    return mysql({
        url: '/mysql/getUserInfo',
        method: 'get',
    });
}

export function getAllNation() {
    return mysql({
        url: '/mysql/getAllNation',
        method: 'get',
    });
}

export function getAllJob() {
    return mysql({
        url: '/mysql/getAllJob',
        method: 'get',
    });
}

export function creatUser(data) {
    return mysql({
        url: '/mysql/creatUser',
        method: 'post',
        data: JSON.stringify(data),
    });
}
