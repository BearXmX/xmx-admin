/*
 * @Description:
 * @Author: mingxiangxiong
 * @Date: 2021-03-13 16:16:17
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-08-01 16:25:02
 */
import request from '../server/request';
export function getUserInfo() {
    return request({
        url: '/get/getUserInfo',
        method: 'get',
    });
}

export function getBarData() {
    return request({
        url: 'get/getBarData',
        method: 'get',
    });
}

export function getLineData() {
    return request({
        url: 'get/getLineData',
        method: 'get',
    });
}

export function getSaleData() {
    return request({
        url: 'get/getSaleData',
        method: 'get',
    });
}
